import './bootstrap';
import '../css/app.css';

import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/inertia-vue3'
import { InertiaProgress } from '@inertiajs/progress'
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers";
import { DefaultApolloClient } from '@vue/apollo-composable'
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'

const httpLink = createHttpLink({
    uri: 'https://full-stack-meeting-room.test/graphql',
})

const cache = new InMemoryCache();


const apolloClient = new ApolloClient({
    link: httpLink,
    cache,
})

createInertiaApp({
    resolve: name => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(plugin)
            .provide(DefaultApolloClient,apolloClient)
            .mount(el)
    },
})

InertiaProgress.init()
