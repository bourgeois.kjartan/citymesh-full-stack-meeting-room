<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bookings')->insert([
            'id' => 1,
            'email' => 'john.doe@test.com',
            'participants' => 2,
            'room_id' => 1,
            'date' => '01/01/2025',
            'start_time' => '11:00',
            'end_time' => '11:30'
        ]);

        DB::table('bookings')->insert([
            'id' => 2,
            'email' => 'jane.doe@test.com',
            'participants' => 3,
            'room_id' => 2,
            'date' => '01/01/2025',
            'start_time' => '08:00',
            'end_time' => '08:30'
        ]);

        DB::table('bookings')->insert([
            'id' => 3,
            'email' => 'james.doe@test.com',
            'participants' => 8,
            'room_id' => 3,
            'date' => '01/01/2025',
            'start_time' => '14:00',
            'end_time' => '14:30'
        ]);

        DB::table('bookings')->insert([
            'id' => 4,
            'email' => 'john.doe@test.com',
            'participants' => 24,
            'room_id' => 4,
            'date' => '02/01/2025',
            'start_time' => '07:00',
            'end_time' => '07:30'
        ]);

        DB::table('bookings')->insert([
            'id' => 5,
            'email' => 'james.doe@test.com',
            'participants' => 2,
            'room_id' => 1,
            'date' => '02/01/2025',
            'start_time' => '09:00',
            'end_time' => '09:30'
        ]);

        DB::table('bookings')->insert([
            'id' => 6,
            'email' => 'james.doe@test.com',
            'participants' => 4,
            'room_id' => 2,
            'date' => '06/01/2025',
            'start_time' => '08:00',
            'end_time' => '08:30'
        ]);

        DB::table('bookings')->insert([
            'id' => 7,
            'email' => 'jill.doe@test.com',
            'participants' => 8,
            'room_id' => 3,
            'date' => '02/01/2025',
            'start_time' => '07:00',
            'end_time' => '07:30'
        ]);

    }
}
